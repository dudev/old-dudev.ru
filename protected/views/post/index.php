<?php if(!empty($_GET['tag'])): ?>
<h1>Записи с тегом <i><?php echo CHtml::encode($_GET['tag']); ?></i></h1>
<?php else: ?>
<h1>Записи</h1>
<?php endif;
//echo CPasswordHelper::hashPassword('Simona3456');
$this->widget('zii.widgets.CListView', array(
    'dataProvider'=>$dataProvider,
    'ajaxUpdate'=>false,
    'itemView'=>'_view',
    'template'=>"{items}{pager}",
)); ?>
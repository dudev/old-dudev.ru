<?php
/* @var $this PostController */
/* @var $model Post */

$this->breadcrumbs=array(
	'Посты'=>array('admin'),
	'Создание поста',
);
?>

<h1>Создать пост</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
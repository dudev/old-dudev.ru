<?php
/* @var $this PostController */
/* @var $model Post */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'post-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
));

Yii::app()->clientScript->registerCss('datepicker', "
#ui-datepicker-div {
    z-index: 5!important;
}
"); ?>

	<?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'category_id'); ?>
        <?php echo $form->dropDownList($model,'category_id',Category::items()); ?>
        <?php echo $form->error($model,'category_id'); ?>
    </div>

    <div class="row">
		<?php echo $form->labelEx($model,'nick'); ?>
		<?php echo $form->textField($model,'nick',array('maxlength'=>128)); ?>
		<?php echo $form->error($model,'nick'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('maxlength'=>128)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'content'); ?>		
		<?php $attribute='content';
		$this->widget('ext.redactor.ImperaviRedactorWidget',array(
		    'model'=>$model,
		    'attribute'=>$attribute,
		    'options'=>array(
                'lang'=>'ru',
		        'fileUpload'=>Yii::app()->createUrl('post/fileUpload',array(
		            'attr'=>$attribute
		        )),
		        'fileUploadErrorCallback'=>new CJavaScriptExpression(
		            'function(obj,json) { alert(json.error); }'
		        ),
		        'imageUpload'=>Yii::app()->createUrl('post/imageUpload',array(
		            'attr'=>$attribute
		        )),
		        'imageGetJson'=>Yii::app()->createUrl('post/imageList',array(
		            'attr'=>$attribute
		        )),
		        'imageUploadErrorCallback'=>new CJavaScriptExpression(
		            'function(obj,json) { alert(json.error); }'
		        ),
		    ),
		));
		?>
				
		<?php echo $form->error($model,'content'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tags'); ?>
		<?php echo $form->textArea($model,'tags'); ?>
		<?php echo $form->error($model,'tags'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',Lookup::items('PostStatus')); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'public_time'); ?>
		<?php $this->widget('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker', [
		   'name' => 'Post[public_time]',
		   'model' => $model,
		   'attribute' => 'public_time',
		   'language' => 'ru',
		   'options' => '',
		]); ?>
		<?php echo $form->error($model,'public_time'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'image'); ?>
		<?php echo CHtml::activeFileField($model, 'image'); ?>
		<?php echo $form->error($model,'image'); ?>
		<?php echo $model->isNewRecord ? '' : CHtml::image('/images/'.$model->id.'-min.jpg?rnd='.rand()); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'is_bigimg_post', ['style'=>'float: left;']); ?>
        <?php $checked = $model->isNewRecord ? ['checked'=>true] : [];
              echo $form->checkBox($model,'is_bigimg_post'); ?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class'=>'button']); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
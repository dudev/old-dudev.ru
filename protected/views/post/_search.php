<?php
/* @var $this PostController */
/* @var $model Post */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tags'); ?>
        <?php echo $form->dropDownList($model,'tags',[''=>'']+Tag::items()); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
        <?php echo $form->dropDownList($model,'status',[''=>'']+Lookup::items('PostStatus')); ?>
	</div>

    <div class="row">
        <?php echo $form->label($model,'category_id'); ?>
        <?php echo $form->dropDownList($model,'category_id',[''=>'']+Category::items()); ?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Поиск', ['class'=>'button']); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
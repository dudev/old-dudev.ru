<?php
/* @var $this PostController */
/* @var $model Post */

$this->breadcrumbs=array(
	'Управление постами',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('.grid-view').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Управление постами</h1>

<?php echo CHtml::link('Расширенный поиск','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    //'summaryText'=>Yii::t('zii','Показаны {start}-{end} из 1 поста.|Показаны {start}-{end} из {count} постов.',$model->search()->getTotalItemCount()),
    'pager'=>[
        'header'=>'',
        'nextPageLabel'=>'&gt;',
        'prevPageLabel'=>'&lt;',
        'firstPageLabel'=>'&lt;&lt;',
        'lastPageLabel'=>'&gt;&gt;',
    ],
    'template'=>'{items}{pager}',
    'columns'=>array(
        array(
            'name'=>'title',
            'type'=>'raw',
            'value'=>'CHtml::link(CHtml::encode($data->title), $data->url)',
            'filter'=>false
        ),
        array(
            'name'=>'status',
            'value'=>'Lookup::item("PostStatus",$data->status)',
            'filter'=>Lookup::items('PostStatus'),
        ),
        array(
            'name'=>'public_time',
            'filter'=>false,
        ),
        array(
            'class'=>'CButtonColumn',
        ),
    ),
)); ?>
<?php
/* @var $this PageController */
/* @var $model Page */

$this->breadcrumbs=array(
	'Страницы'=>array('index'),
	'Изменение страницы',
);
?>

<h1>Изменение страницы</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
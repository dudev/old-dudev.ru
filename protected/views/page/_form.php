<?php
/* @var $this PageController */
/* @var $model Page */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'page-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nick'); ?>
		<?php echo $form->textField($model,'nick',array('maxlength'=>128)); ?>
		<?php echo $form->error($model,'nick'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('maxlength'=>128)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'content'); ?>
        <?php $attribute='content';
        $this->widget('ext.redactor.ImperaviRedactorWidget',array(
            'model'=>$model,
            'attribute'=>$attribute,
            'options'=>array(
                'lang'=>'ru',
                'fileUpload'=>Yii::app()->createUrl('page/fileUpload',array(
                        'attr'=>$attribute
                    )),
                'fileUploadErrorCallback'=>new CJavaScriptExpression(
                        'function(obj,json) { alert(json.error); }'
                    ),
                'imageUpload'=>Yii::app()->createUrl('page/imageUpload',array(
                        'attr'=>$attribute
                    )),
                'imageGetJson'=>Yii::app()->createUrl('page/imageList',array(
                        'attr'=>$attribute
                    )),
                'imageUploadErrorCallback'=>new CJavaScriptExpression(
                        'function(obj,json) { alert(json.error); }'
                    ),
            ),
        ));
        ?>
		<?php echo $form->error($model,'content'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model,'status',[''=>'']+Lookup::items('PageStatus')); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class'=>'button']); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
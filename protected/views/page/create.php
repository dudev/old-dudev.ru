<?php
/* @var $this PageController */
/* @var $model Page */

$this->breadcrumbs=array(
	'Страницы'=>array('index'),
	'Создание страницы',
);
?>

<h1>Создание страницы</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
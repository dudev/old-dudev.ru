<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'comment-form',
    'enableAjaxValidation'=>true,
)); ?>

	<?php echo $form->errorSummary($model); ?>
	
	<label><?php echo $form->labelEx($model,'author'); ?></label>
	<?php echo $form->textField($model,'author',array('maxlength'=>128)); ?>
	<?php echo $form->error($model,'author'); ?>
	
	<label><?php echo $form->labelEx($model,'email'); ?></label>
	<?php echo $form->textField($model,'email',array('maxlength'=>128)); ?>
	<?php echo $form->error($model,'email'); ?>
	
	<label><?php echo $form->labelEx($model,'url'); ?></label>
	<?php echo $form->textField($model,'url',array('maxlength'=>128)); ?>
	<?php echo $form->error($model,'url'); ?>
	
	<label><?php echo $form->labelEx($model,'content'); ?></label>
	<?php //echo $form->textArea($model,'content'); ?>
    <?php $attribute='content';
    $this->widget('ext.redactor.ImperaviRedactorWidget',array(
        'model'=>$model,
        'attribute'=>$attribute,
        'options'=>array(
            'lang'=>'ru',
            'fileUploadErrorCallback'=>new CJavaScriptExpression(
                    'function(obj,json) { alert(json.error); }'
                ),
            'imageUpload'=>Yii::app()->createUrl('post/imageUpload',array(
                    'attr'=>$attribute
                )),
            'imageGetJson'=>Yii::app()->createUrl('post/imageList',array(
                    'attr'=>$attribute
                )),
            'imageUploadErrorCallback'=>new CJavaScriptExpression(
                    'function(obj,json) { alert(json.error); }'
                ),
        ),
    ));
    ?>
	<?php echo $form->error($model,'content'); ?>

    <!--label--><?php /*echo $form->labelEx($model,'status'); ?></label>
    <?php echo $form->dropDownList($model,'status',Lookup::items('CommentStatus')); ?>
    <?php echo $form->error($model,'status');*/ ?>
	
	<?php echo CHtml::submitButton($model->isNewRecord ? 'Отправить' : 'Сохранить', ['class'=>'button']); ?>

<?php $this->endWidget(); ?>

</div><!-- form -->
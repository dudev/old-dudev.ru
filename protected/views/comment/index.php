<?php
/* @var $this CommentController */
/* @var $model Comment */

$this->breadcrumbs=array(
    'Управление комментариями',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#comment-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});");
?>

<h1>Управление комментариями</h1>

<?php echo CHtml::link('Расширенный поиск','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search',array(
        'model'=>$model,
    )); ?>
</div><!-- search-form -->

<?php
//задаём количество строк на странице
$dataProvider = $model->search();
$dataProvider->setPagination(['pageSize'=>20]);

$this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'comment-grid',
    'dataProvider'=>$dataProvider,
    'filter'=>$model,
    //'summaryText'=>Yii::t('zii','Показаны {start}-{end} из 1 комментария.|Показаны {start}-{end} из {count} комментариев.',$model->search()->getTotalItemCount()),
    'pager'=>[
        'class'=>'CLinkPager',
        'header'=>'',
        'nextPageLabel'=>'&gt;',
        'prevPageLabel'=>'&lt;',
        'firstPageLabel'=>'&lt;&lt;',
        'lastPageLabel'=>'&gt;&gt;',
        'pageSize'=>20,
    ],
    'template'=>'{items}{pager}',
    'columns'=>array(
        [
            'name'=>'post.title',
            'type'=>'raw',
            'value'=>'CHtml::link(CHtml::encode($data->post->title), $data->post->url)',
            'header'=>Yii::t('comment', 'К посту'),
            'filter'=>false,
        ],
        array(
            'name'=>'content',
            'value'=>'Controller::shortContent($data->content, 150);',
            'filter'=>false,
            'htmlOptions'=>[
                'style'=>'width:320px;word-wrap: break-word;word-break: break-word;'
            ],
        ),
        array(
            'name'=>'status',
            'value'=>'Lookup::item("CommentStatus",$data->status)',
            'filter'=>Lookup::items('CommentStatus'),
            'htmlOptions'=>[
                'style'=>'width:90px;'
            ],
        ),
        array(
            'name'=>'create_time',
            'filter'=>false,
            'htmlOptions'=>[
                'style'=>'width:92px;'
            ],
        ),
        [
            'class'=>'CButtonColumn',
            'template' => '{approve}{update}{delete}',
            'buttons'=>
            [
                'approve' =>
                [
                    'label' => 'Одобрить',
                    'imageUrl' => '/i/approve.png',
                    'url' => 'Yii::app()->createUrl("comment/approve", array("id"=>$data->id))',
                    'options'=>['class'=>'approve'],
                    'visible'=>'$data->status==1',
                    'click'=><<<EOD
function() {
	if(!confirm('Одобрить комментарий?')) return false;
	jQuery('#comment-grid').yiiGridView('update', {
		type: 'POST',
		url: jQuery(this).attr('href'),
		success: function(data) {
			jQuery('#comment-grid').yiiGridView('update');
		},
	});
	return false;
}
EOD
                ],
            ],
            'htmlOptions'=>[
                'style'=>'width:76px;'
            ],
        ],
    ),
)); ?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'comment-form',
    'enableAjaxValidation'=>true,
)); ?>

	<?php echo $form->errorSummary($model); ?>
	
	<label><?php echo $form->labelEx($model,'author'); ?></label>
	<?php echo $form->textField($model,'author',array('maxlength'=>128)); ?>
	<?php echo $form->error($model,'author'); ?>
	
	<label><?php echo $form->labelEx($model,'email'); ?></label>
	<?php echo $form->textField($model,'email',array('maxlength'=>128)); ?>
	<?php echo $form->error($model,'email'); ?>
	
	<label><?php echo $form->labelEx($model,'url'); ?></label>
	<?php echo $form->textField($model,'url',array('maxlength'=>128)); ?>
	<?php echo $form->error($model,'url'); ?>
	
	<label><?php echo $form->labelEx($model,'content'); ?></label>
	<?php //echo $form->textArea($model,'content'); ?>
    <?php $this->widget('ext.markitup.EMarkitupWidget', array(
        // можно использовать как для поля модели
        'model' => $model,
        'attribute' => 'content',
        'settings'=>'bbcode',

        // так и просто для элемента формы
        'name' => 'Comment[content]',
    ))?>
	<?php echo $form->error($model,'content'); ?>

    <!--label--><?php /*echo $form->labelEx($model,'status'); ?></label>
    <?php echo $form->dropDownList($model,'status',Lookup::items('CommentStatus')); ?>
    <?php echo $form->error($model,'status');*/ ?>
	
	<?php echo CHtml::submitButton($model->isNewRecord ? 'Отправить' : 'Сохранить', ['class'=>'button']); ?>

<?php $this->endWidget(); ?>

</div><!-- form -->
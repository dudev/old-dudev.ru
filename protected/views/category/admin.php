<?php
/* @var $this CategoryController */
/* @var $model Category */

$this->breadcrumbs=array(
	'Разделы'=>array('index'),
	'Управление разделами',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$('#category-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Управление разделами</h1>

<?php echo CHtml::link('Создать раздел',Yii::app()->createUrl('category/create')); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'category-grid',
	'dataProvider'=>$model->search(),
    'pager'=>[
        'class'=>'CLinkPager',
        'header'=>'',
        'nextPageLabel'=>'&gt;',
        'prevPageLabel'=>'&lt;',
        'firstPageLabel'=>'&lt;&lt;',
        'lastPageLabel'=>'&gt;&gt;',
        'pageSize'=>20,
    ],
    'template'=>'{items}{pager}',
	'columns'=>array(
		'name',
		array(
			'class'=>'CButtonColumn',
            'template'=>'{update}{delete}',
		),
	),
)); ?>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" rel="stylesheet" type="text/css">
</head>
<body>
	<header>
  	    	<a href="/"><?php echo CHtml::encode(Yii::app()->name); ?></a>
	</header>
	<div id="page">
		<?php if(isset($this->breadcrumbs)):?>
			<?php $this->widget('zii.widgets.CBreadcrumbs', array(
				'links'=>$this->breadcrumbs,
				'homeLink'=>CHtml::link('Главная', Yii::app()->homeUrl),
			)); ?>
		<?php endif?>
		<div id="content">
			<?php echo $content; ?>
		</div>
		<div id="sidebar">

		    <?php if(!Yii::app()->user->isGuest) $this->widget('UserMenu'); ?>



		    <?php
			/*<h4>Поиск</h4>
			<form action="#" class="s">
				<input id="search" type="text" value="Я ищу...">
			</form>*/
			?>
			<nav>
				<h4>Меню</h4>
				<ul>
					<li><a href="/">Блог</a></li>
                    <?php $this->widget('PageMenu'); ?>
				</ul>
                <?php $this->widget('CategoryMenu'); ?>
			</nav>
            <?php if($this->beginCache('tagCloud', array('duration'=>3600*24))) { ?>
		        <?php $this->widget('TagCloud', ['min_frequency' => 2]); ?>
            <?php $this->endCache(); } ?>

		</div>
	</div>
	<footer>
		<p>&copy;&nbsp;2008 -  <?= date('Y') ?>&nbsp;Русанов Евгений</p>
	</footer>
</div>
<!-- Yandex.Metrika counter --><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter21505294 = new Ya.Metrika({id:21505294, webvisor:true, clickmap:true, trackLinks:true, accurateTrackBounce:true}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="//mc.yandex.ru/watch/21505294" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-52660567-1', 'auto');
        ga('send', 'pageview');

    </script>
</body>
</html>
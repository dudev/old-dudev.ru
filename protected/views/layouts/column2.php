<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div id="content">
		<?php echo $content; ?>
</div>
<div id="sidebar">

    <?php if(!Yii::app()->user->isGuest) $this->widget('UserMenu'); ?>

    <?php
	/*<h4>Поиск</h4>
	<form action="#" class="s">
		<input id="search" type="text" value="Я ищу...">
	</form>*/
	?>
	<nav>
		<h4>Меню</h4>
		<ul>
			<li><a href="http://dudev.ru/">Блог</a></li>
			<li><a href="http://dudev.ru/about-me.html">Обо мне</a></li>
			<li><a href="http://dudev.ru/offers.html">Услуги</a></li>
			<li><a href="http://dudev.ru/contacts.html">Контакты</a></li>
		</ul>
		<!--//<h4>Разделы</h4>
		<ul>
			{foreach from=$menu item="cat"}
				<li><a href="http://dudev.ru/blog/cat/{$cat["id"]}/">{$cat["name"]}</a></li>
			{/foreach}
		</ul>//-->
	</nav>

    <?php $this->widget('TagCloud'); ?>

</div>
<?php $this->endContent(); ?>
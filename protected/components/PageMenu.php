<?php
Yii::import('zii.widgets.CPortlet');

class PageMenu extends CPortlet
{
    /*public function init()
    {
        $this->title=CHtml::encode(Yii::app()->user->name);
        parent::init();
    }*/

    protected function renderContent()
    {
        $pages = Yii::app()->db->createCommand()
            ->select('nick, title')
            ->from('{{page}}')
            ->where('status = ' . Page::STATUS_PUBLISHED)
            ->queryAll();
        $this->render('pageMenu', ['pages'=>$pages]);
    }
}
<?php
Yii::import('zii.widgets.CPortlet');

class CategoryMenu extends CPortlet
{
    public $title='Разделы';

    protected function renderContent()
    {
        echo '<nav><ul>';
        foreach(Category::model()->items() as $category_id=>$name)
        {
            $link=CHtml::link(CHtml::encode($name), array('post/category','category_id'=>$category_id));
            echo CHtml::tag('li', [], $link);
        }
        echo '</ul></nav>';
    }
}
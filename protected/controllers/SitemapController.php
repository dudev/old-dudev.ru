<?php

class SitemapController extends Controller
{
	public function actionIndex()
	{

        if (!$xml = Yii::app()->cache->get('sitemap'))
        {
            $sitemap = new SitemapLogic();
            $sitemap->addUrl('/', SitemapLogic::DAILY, 1);
            $sitemap->addUrlsByModel(Post::model(), SitemapLogic::WEEKLY, 0.8);
            $sitemap->addModel(Page::model()->published()->findAll(), SitemapLogic::MONTHLY, 0.5);

            $xml = $sitemap->render();
            Yii::app()->cache->set('sitemap', $xml, 3600*24);
        }

        header("Content-type: text/xml");
        echo $xml;
        Yii::app()->end();
	}
}
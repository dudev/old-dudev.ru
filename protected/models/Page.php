<?php

/**
 * This is the model class for table "{{page}}".
 *
 * The followings are the available columns in table '{{page}}':
 * @property integer $id
 * @property string $nick
 * @property string $title
 * @property string $content
 * @property integer $status
 * @property integer $create_time
 * @property integer $update_time
 */
class Page extends CActiveRecord
{
    const STATUS_DRAFT=1;
    const STATUS_PUBLISHED=2;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{page}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, content, status', 'required', 'message'=>'{attribute} не может быть пустым.'),
            array('nick, title', 'length', 'max'=>128),
            ['nick', 'translit'],
			array('status', 'in', 'range'=>[1,2]),
		);
	}

    public function translit($attribute,$params){
        if(empty($this->nick)) {
            $this->nick=$this->title;
        }
        $this->nick=Controller::translit($this->nick);
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

    public function scopes()
    {
        return array(
            'published'=>array(
                'condition'=>'status='.Page::STATUS_PUBLISHED,
            ),
        );
    }

    public function byNick($nick)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=>'nick=:nick',
            'params'=>['nick'=>$nick]
        ));
        return $this;
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nick' => 'Мнемоника',
			'title' => 'Название',
			'content' => 'Текст',
			'status' => 'Статус',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Page the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    protected function beforeSave() {
        if(parent::beforeSave()) {
            if($this->isNewRecord)
                $this->create_time=$this->update_time=time();
            else
                $this->update_time=time();
            return true;
        }
        else {
            return false;
        }
    }

    public function getUrl()
    {
        return Yii::app()->createUrl('page/view', array(
            'nick'=>$this->nick,
        ));
    }
}

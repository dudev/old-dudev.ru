<?php

/**
 * This is the model class for table "{{post}}".
 *
 * The followings are the available columns in table '{{post}}':
 * @property integer $id
 * @property string $nick
 * @property string $title
 * @property string $content
 * @property string $tags
 * @property integer $status
 * @property integer $create_time
 * @property integer $public_time
 * @property integer $update_time
 * @property integer $author_id
 *
 * The followings are the available model relations:
 * @property Comment[] $comments
 * @property User $author
 */
class Post extends CActiveRecord
{
	const STATUS_DRAFT=1;
    const STATUS_PUBLISHED=2;
    const STATUS_ARCHIVED=3;
    
    public $image;

	private $_oldTags;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{post}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
            ['title, content, status, tags', 'required', 'message'=>'{attribute} не может быть пустым.'],
			['title, nick', 'length', 'max' => 128],
            ['nick', 'translit'],
	        ['status', 'in', 'range' => [1, 2, 3] ],
			['tags', 'match', 'pattern'=>'/^[\w\s,А-Яа-я]+$/u', 'message'=>'В тегах можно использовать только буквы.'],
	        ['tags', 'normalizeTags'],
	        ['public_time', 'date', 'format'=>'dd.MM.yyyy HH:mm'],
	        ['image', 'length', 'max' => 255],
            ['image', 'file',
                'on'=>'update',
                'types'=>'jpg, gif, png, jpeg',
                'maxSize'=>1024 * 1024 * 5, // 5 MB
                'allowEmpty'=>true,
                'tooLarge'=>'Файл весит больше 5 MB. Пожалуйста, загрузите файл меньшего размера.',
            ],
            ['image', 'file',
                'on'=>'create',
                'types'=>'jpg, gif, png, jpeg',
                'maxSize'=>1024 * 1024 * 5, // 5 MB
                'allowEmpty'=>false,
                'tooLarge'=>'Файл весит больше 5 MB. Пожалуйста, загрузите файл меньшего размера.',
            ],
            ['category_id', 'numerical'],
            ['is_bigimg_post', 'boolean'],
			['id, tags, status, category_id', 'safe', 'on'=>'search'],
		];
	}

    public function translit($attribute,$params){
        if(empty($this->nick)) {
            $this->nick=$this->title;
        }
        $this->nick=Controller::translit($this->nick);
    }

	public function normalizeTags($attribute,$params)
	{
	    $this->tags=Tag::array2string(array_unique(Tag::string2array($this->tags)));
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
	        'author' => [self::BELONGS_TO, 'User', 'author_id'],
            'category' => [self::BELONGS_TO, 'Category', 'category_id'],
	        'comments' => [self::HAS_MANY, 'Comment', 'post_id',
	            'condition'=>'comments.status='.Comment::STATUS_APPROVED,
	            'order'=>'comments.create_time DESC'],
	        'commentCount' => [self::STAT, 'Comment', 'post_id',
	            'condition'=>'status='.Comment::STATUS_APPROVED],
	    ];
	}

    public function scopes()
    {
        return array(
            'published'=>array(
                'condition'=>'t.status='.Post::STATUS_PUBLISHED.' and t.public_time < ' . time(),
            ),
        );
    }
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'id'=>'ID',
            'category_id'=>'Раздел',
			'nick' => 'Ник',
			'title' => 'Название',
			'content' => 'Текст',
			'tags' => 'Теги',
			'status' => 'Статус',
			'public_time' => 'Дата публикации',
			'image' => 'Картинка',
            'is_bigimg_post'=>'Показывать картинку внутри поста',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tags',$this->tags,true);
        $criteria->compare('status',$this->status);
        $criteria->compare('category_id',$this->category_id);

		return new CActiveDataProvider($this, array(
            'pagination'=>
                ['pageSize'=>20],
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 't.create_time DESC',
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Post the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	protected function beforeSave() {
		if(parent::beforeSave()) {
            if(empty($this->public_time)) {
                $this->public_time=time();
            }
            else {
                $this->public_time = strtotime($this->public_time);
            }
            if($this->isNewRecord)
	        {
	            $this->create_time=$this->update_time=time();
	            $this->author_id=Yii::app()->user->id;
	        }
	        else
	            $this->update_time=time();
			return true;
		}
		else {
			return false;
		}
	}

	protected function afterSave()
	{
	    parent::afterSave();
	    Tag::model()->updateFrequency($this->_oldTags, $this->tags);
	}

 	protected function afterFind() {
 		parent::afterFind();
		$this->public_time = date('d.m.Y H:i', $this->public_time);
		$this->_oldTags=$this->tags;
	}

	protected function afterDelete()
	{
	    parent::afterDelete();
	    //Comment::model()->deleteAll('post_id='.$this->id);
	    Tag::model()->updateFrequency($this->tags, '');
	}

	public function getUrl()
    {
        return Yii::app()->createUrl('post/view', array(
            'id'=>$this->id,
            'nick'=>$this->nick,
        ));
    }

    public function addComment($comment)
	{
	    if(Yii::app()->params['commentNeedApproval'])
	        $comment->status=Comment::STATUS_PENDING;
	    else
	        $comment->status=Comment::STATUS_APPROVED;
	    $comment->post_id=$this->id;
	    return $comment->save();
	}

    public function getUrlsForSitemap() {
        foreach($this->published()->findAll() as $model) {
            $lastCommentCreate = Yii::app()->db->createCommand()
                ->select('create_time')
                ->from('{{comment}}')
                ->where('post_id=:post_id and status='.Comment::STATUS_APPROVED, array(':post_id'=>$model->id))
                ->order('create_time desc')
                ->limit(1)
                ->queryScalar();
            if($lastCommentCreate > $model->update_time) {
                $urls[$model->getUrl()] = $lastCommentCreate;
            }
            else{
                $urls[$model->getUrl()] = $model->update_time;
            }
        }
        return $urls;
    }
}

<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Евгений Русанов :: Блог программиста',

	// preloading 'log' component
	'preload'=>array('log'),

	'defaultController'=>'post',

    'sourceLanguage' => 'ru',
	
	'charset'=>'UTF-8',

	// application components
	'components'=>array(
        //использование ссылок, для файлов ресурсов
        /*'assetManager' => array(
            'linkAssets' => true,
        ),*/
        'cache'=>
        [
            'class'=>'system.caching.CFileCache'//CDummyCache
        ],
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		'ih'=>array('class'=>'CImageHandler'),
		/*'db'=>array(
            'connectionString' => 'mysql:host=localhost;dbname=dudev',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
            'tablePrefix' => 'tbl_',
        ),*/
        'db'=>array(
            'connectionString' => 'mysql:host=localhost;dbname=dudev_dudev-ru',
            'emulatePrepare' => true,
            'username' => '045647333_dudevr',
            'password' => 'd9pFx2.{(bMBKG3T',
            'charset' => 'utf8',
            'tablePrefix' => 'tbl_',
            'schemaCachingDuration'=>3600,
            /*'enableProfiling'=>true,
            'enableParamLogging'=>true,*/
        ),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				/*array(
					'class'=>'CWebLogRoute',
					'levels'=>'error, warning',
				),*/
                array(
                    'class'=>'CEmailLogRoute',
                    'levels'=>'error, warning',
                    'emails'=>'admin@dudev.ru',
                ),
                /*array(
                    'class'=>'ext.yii-debug-toolbar-master.YiiDebugToolbarRoute',
                    // Access is restricted by default to the localhost
                    'ipFilters'=>array('212.193.87.33'),
                ),*/

			),
		),
		'urlManager'=>array(
            'urlFormat'=>'path',
            'showScriptName'=>false,
            'rules'=>array(
                'blog/<id:\d+>_<nick:.*?>.html'=>'post/view',
                'blog/tags/<tag:.*?>'=>'post/index',
                'blog'=>'post/index',
                'blog/post/update/<id:\d+>'=>'post/update',
                'blog/cat/<category_id:\d+>'=>'post/category',
                '<nick:[\w-_]+>.html'=>'page/view',
                ['sitemap/index', 'pattern'=>'sitemap.xml', 'urlSuffix'=>''],
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
            ),
        ),
        'decoda' => array(
            'class' => 'ext.decoda.YiiDecoda',
            'defaults' => true,
            'locale' => 'ru',
            'convertWhitespaces'=>false,
        ),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'admin@dudev.ru',
		'tagCloudCount'=>'20',
		'commentNeedApproval'=>true,

	),
	'import'=>array(
        'application.models.*',
        'application.components.*',
    ),

    /*'modules'=>array(
        'gii'=>array(
            'class'=>'system.gii.GiiModule',
            'password'=>'123456',
            'ipFilters'=>['212.193.87.33'],
        ),
    ),*/
);